


function doLogin( email,  password){
    var xhr = new XMLHttpRequest();
    ///auth/login
    xhr.open("POST", env.prod+'/auth/login', true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
        username:email,
            password:password
    }));
    xhr.onload = function() {
        if(this.status===403){
            console.log('error');
        }
        if(this.status===200){
            var data = JSON.parse(this.responseText);
            console.log(data);
            if (typeof(Storage) !== "undefined") {
              localStorage.setItem("user", JSON.stringify(data.body.user));
              localStorage.setItem("token", data.body.token);
            } else {
              // Sorry! No Web Storage support..
            }
            switch (data.body.user.rol.id) {
              case 2:
                document.location.href = './views/menu-staff.html';
                break;
              case 3:
                document.location.href = './views/menu-doctor.html';
                break;
              default:
                break;
            }

            
        }
        
    }
    xhr.onerror = function() { alert('Error de conexion'); };

}
(function() {
    'use strict';
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      //document.location.href = './views/menu-staff.html';
      //return;
      var forms = document.getElementsByClassName('needs-validation');
      var s_label = document.getElementById('s-label');
      
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
            
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
          if (form.checkValidity() === true) {
            //todo call service
            event.preventDefault();
            s_label.value = "Iniciando...";
            s_label.classList.add('spinner-border');
            
            doLogin(document.getElementById("i-email").value,document.getElementById("i-password").value);
            
          }
            
          
        }, false);
      });
    }, false);
  })();