const INDEX_G = 1;
const MAX_LENGTH_DNI = 8;
const MIN_LENGTH_MEDICINE = 7;
//const API ="http://localhost:8080/api/v1/";
const API = "https://api-rest-tesis2.herokuapp.com/api/v1/";
const FAKE_API = "http://localhost:3000/";
const TOKEN = localStorage.getItem("token").replace(/"/g, "");
var table = document.getElementById("t-appointments");
var table_availability = document.getElementById("t-availability");
var t_availability_result = document.getElementById("t-availability-result");

var inputDni = document.getElementById("inputDni");
var inputMedicine = document.getElementById("codemedicine");
var medicinename = document.getElementById("medicinename");
var medicineLoader = document.getElementById("medicineLoader");
medicineLoader.hidden = true;

var inputUser = document.getElementById("inputUser");
var inputDoctor = document.getElementById("inputDoctor");

var user =JSON.parse(localStorage.getItem("user"));

inputDoctor.value= user.firstName +" "+ user.lastName;
var userLoader = document.getElementById("userLoader");
userLoader.hidden = true;


var appointment_tab  = document.getElementById("appointment-tab");
var inputDate  = document.getElementById("inputDate");
var save_record  = document.getElementById("save-record");




const selectSpecialties = document.getElementById("selectSpecialties");
const selectDoctors = document.getElementById("selectDoctors");

var list_data_doctors = [];

var spec_selected = 0;
var doc_selected = 0;

function reserve() {
  $('#md-close').trigger('click');
  console.log('reserve');
  //use data
  //call service
  
  //conAppModal.modal('toggle');
  
}

//Set listeners
inputDni.addEventListener('keyup', getUser);
inputMedicine.addEventListener('keyup', getMedicine);


save_record.addEventListener('click',(event)=>{
  console.log("calling....save_record");
  //Validar usuario
  console.log( localStorage.getItem("registro_receta"));
  if( localStorage.getItem("registro_receta") != "true") return;
  if( localStorage.getItem("registro_medicina") != "true") return;

  var description  = document.getElementById("result-consult");
  var  interval_h  = document.getElementById("interval-h");
  var  interval_d  = document.getElementById("interval-d");
  if(description=="" || interval_d == "" || interval_h == "")return;
  if(inputDate.value=="")return;
  var dt= inputDate.value.split('-');
  var dtd = dt[2]+'/'+dt[1]+'/'+dt[0];
 
  var usr = JSON.parse( localStorage.getItem("registro_receta_user"));
  var doctor = JSON.parse( localStorage.getItem("user"));
  var medicine = JSON.parse( localStorage.getItem("registro_medico_medicina"));
  console.log(usr.id);
  //Store in WS
  var xhr = new XMLHttpRequest();
  xhr.open("POST", API+'medicalregister', true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.setRequestHeader('Authorization', 'Bearer ' + TOKEN);
  xhr.send(JSON.stringify({
      appointment_id:usr.last_appointment_id,
      user_id:usr.id,
      doctor_id:doctor.id,
      medicine_id:medicine.id,
      comments:description.value,
      prescription:"Cada "+interval_h.value+ " horas x por "+interval_d.value+" días",
      date:dtd,
      name:"RECETA MÉDICA"


  }));
  xhr.onload = function() {
    if(this.status===403){
        console.log('error');
    }
    if(this.status===200){
        var data = JSON.parse(this.responseText);
        console.log(data);
        if (data !== "undefined") {
         alert('Guardado con éxito');
        location.reload();
        } else {
          alert('Error de conexion');
        }

        
    }
    
}
xhr.onerror = function() { alert('Error de conexion'); };
 
  //localStorage.setItem("registro_receta",true);
  //localStorage.setItem("registro_receta_user", JSON.stringify(data));
});
/*
appointment_tab.addEventListener('click',(event)=>{
  var dt= inputDate.value.split('-');
  var dtd = dt[2]+'/'+dt[1]+'/'+dt[0];
  $('#day_selected').text("Cupos Disponibles para el : Jueves "+dtd);
  var spec_name = $("#selectSpecialties option[value='"+spec_selected+"']").text();
  $('#day_detail').text(spec_name+" - "+doc_selected.name);
  
  var dummy_list = [];
  let init = Math.floor(Math.random()*(6)+7);
  for (let index = init; index < init+6; index++) {
    
      var sched = {
        a:(index<10?'0'+index:index)+':00',
        b:((index+1)<10?'0'+(index+1):(index+1))+':00',
        c:'709',
        d:'PISO 7'
      }
      dummy_list.push(sched);
  }

  dummy_list.forEach(appointment => {
    var row = t_availability_result.insertRow(t_availability_result.rows.length);
    row.value = appointment.id;
    row.className ="tr-alt";
    //adding first 4 td
    Object.keys(appointment).forEach(e => {
      var cell = row.insertCell(row.cells.length);
      cell.innerHTML = appointment[e];
      cell.className="td-alt";
    });
    //adding the 5th
    var cell = row.insertCell(row.cells.length);
    cell.innerHTML = '<button class="btn btn-secondary btn-alt" data-toggle="modal" data-target="#conAppModal" >Reservar</button>';

  });
  t_availability_result.deleteRow(INDEX_G);
    $('.btn-alt').click(function () {
      //this action get the data to reserve appointment and show modal
      var list_content = $(this).closest("tr").find(".td-alt");
      for (let index = 0; index < list_content.length; index++) {
        console.log(list_content[index].innerHTML);
      }
      
         // Finds the closest row <tr> 
         // Gets a descendent with class="nr"
      
    });
    

});
*/
/*
selectSpecialties.addEventListener('change', (event) => {
  if(event.target.value.length>3)return;
  spec_selected = event.target.value;
  var xhr = new XMLHttpRequest();
  //xhr.open("GET", API+'doctor/specialty/'+event.target.value, true);
  xhr.open("GET", FAKE_API+'doctors?specialty='+event.target.value, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.setRequestHeader('Authorization', 'Bearer ' + TOKEN);
  xhr.send(); 
  xhr.onload = function() {
    userLoader.hidden = true;
    if(this.status===403){
        console.log('error');
    }
    if(this.status===200){
      let data = JSON.parse(this.responseText);
      
      if (typeof data !== 'undefined' && data.length > 0) {
        cleanSelectDoctor();
        list_data_doctors = data;
        for (var item of data) {
          var e = document.createElement("option");
          e.innerText = item.name;
          e.value = item.id;
          selectDoctors.append(e);
        }
      }
    }
    
  }
  
});
*/
function fillTableSchedules(data) {
  if (data.length === 0 || data ==="" || data==NaN){
    return;
  }

  data.forEach(appointment => {
    var row = table_availability.insertRow(table_availability.rows.length);
    row.value = appointment.id;
    Object.keys(appointment).forEach(e => {
      var cell = row.insertCell(row.cells.length);
      cell.innerHTML = appointment[e];
    });
    row.classList += "clickable-row";
  });
  table_availability.deleteRow(INDEX_G);
}

function cleanSelectDoctor(){
  let size = selectDoctors.options.length;
  for ( i = size-1; i > 0; i--) {
    selectDoctors.options[i] = null;
  }
}
function fillSpecialites() {
  console.log("calling ... fillSpecialites");
  var xhr = new XMLHttpRequest();
  //xhr.open("GET", FAKE_API+'specialties', true);
  xhr.open("GET", API+'specialties', true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.setRequestHeader('Authorization', 'Bearer ' + TOKEN);
  xhr.send();
  xhr.onload = function() {
    if(this.status===403){
    }
    if(this.status===200){
      let data = JSON.parse(this.responseText);
      
      if (typeof data !== 'undefined' && data.length > 0) {
        
        for (var item of data) {
          var e = document.createElement("option");
          e.innerText = item.title;
          e.value = item.id;
          selectSpecialties.append(e);
        }
      }
    }
    
  } 
}  

function getMedicine(){
  if(inputMedicine.value.length == MIN_LENGTH_MEDICINE){
    medicineLoader.hidden = false;
    queryMedicineDb(inputMedicine.value,medicinename,medicineLoader);
  }
  
}
function getUser(){
  if(inputDni.value.length == MAX_LENGTH_DNI){
    userLoader.hidden = false;
    queryUserDb(inputDni.value,inputUser,userLoader);
  }
  
}
function getUserModal(){
  if(inputDniModal.value.length == MAX_LENGTH_DNI){
    userLoaderModal.hidden = false;
    queryUserDb(inputDniModal.value,inputUserModal,userLoaderModal);
  }
  
}

function queryMedicineDb(medicine,out,loader){
  console.log('queryMedicineDb...running');
  var xhr = new XMLHttpRequest();
  xhr.open("GET", API+'medicine/code/'+medicine, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.setRequestHeader('Authorization', 'Bearer ' + TOKEN);
  xhr.send(); 
  xhr.onload = function() {
    loader.hidden = true;
    if(this.status===403){
        alert('Error de consulta');
       
    }
    if(this.status===200){
      let data = JSON.parse(this.responseText);
      localStorage.setItem("registro_medicina",true);
      localStorage.setItem("registro_medico_medicina", JSON.stringify(data));
      if (typeof data !== 'undefined') {
        out.value = data.name;
      }else{
        alert('No se encontró el paciente');
      }
    }
    
  }
}
function queryUserDb(dni,out,loader) {
  console.log('queryUserDb...running');
  var xhr = new XMLHttpRequest();
  xhr.open("GET", API+'patients/dni/'+dni, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.setRequestHeader('Authorization', 'Bearer ' + TOKEN);
  xhr.send(); 
  xhr.onload = function() {
    loader.hidden = true;
    if(this.status===403){
        alert('Error de consulta');
       
    }
    if(this.status===200){
      let data = JSON.parse(this.responseText);
      localStorage.setItem("registro_receta",true);
      localStorage.setItem("registro_receta_user", JSON.stringify(data));
      if (typeof data !== 'undefined') {
        out.value = data.name;
      }else{
        alert('No se encontró el paciente');
      }
    }
    
  }
}

function searchAppointments(dni,label) {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", env.local+'/appointment/dni/'+dni, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.setRequestHeader('Authorization', 'Bearer ' + TOKEN);
  xhr.send();
  xhr.onload = function() {
      if(this.status===403){
          console.log('error');
      }
      if(this.status===200){
          let data = JSON.parse(this.responseText);
          label.value = "Buscar";
          label.classList.remove('spinner-border');
          
          fillAppointmentTable(data);
          table.deleteRow(INDEX_G);
          
      }
      
  }
  xhr.onerror = function() { alert('Error de conexion'); };
  
}
function fillDocumentsModal(data) {
  let table = document.getElementById("t-modal-documents");
  console.log(table);
   //remove when services are ready
   data.forEach(element => {
    delete element.appointment;
  });
  let count = 1;
  data.forEach(appointment => {
    console.log(appointment);
    var tr = table.insertRow();
    var td1 = tr.insertCell();
    td1.appendChild(document.createTextNode(count));
    var td2 = tr.insertCell();
    td2.appendChild(document.createTextNode(appointment.name));
    var td3 = tr.insertCell();
    //td3.appendChild('<a href="http://www.africau.edu/images/default/sample.pdf" type="button" class="btn btn-primary" target="_blank"><i class="far fa-eye"></i></a>');
    var createA = document.createElement('a');
    createA.className="btn btn-primary";
    var createAText = document.createTextNode("Documento");
    //var href = document.createElement('a');
    createA.setAttribute('href', appointment.url);
    createA.appendChild(createAText);
    td3.appendChild(createA);

    count++;
    
  });
}
function getDocumentsByAppointments(id_appointment) {
  console.log("calling.... getDocumentsByAppointments");
  //TODO set up service {documents in modal}
  var xhr = new XMLHttpRequest();
  xhr.open("GET", API+'records/appointment/'+id_appointment,true);
  //xhr.open("GET", 'http://localhost:3000/documents?appointment='+id_appointment,true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.setRequestHeader('Authorization', 'Bearer ' + TOKEN);
  xhr.send();
  xhr.onload = function() {
      if(this.status===403){
          console.log('error');
      }
      if(this.status===200){
          let data = JSON.parse(this.responseText);
          
          if (data.length === 0 || data ==="" || data==NaN){
            console.log(' is empty');
            $('#moda_empty_documents').modal('toggle');
            
          }else{
            fillDocumentsModal(data);
            $('#registersModal').modal('toggle');
          }

      }
      
  }
  xhr.onerror = function() { alert('Error de conexion'); };
  
}

function fillAppointmentTable(data){
  if (data.length === 0 || data ==="" || data==NaN){
    alert('No hay información disponible');
  }

  //remove when services are ready
  data.forEach(element => {
    delete element.dni;
  });
 
  data.forEach(appointment => {
   
    var row = table.insertRow(table.rows.length);
    row.value = appointment.id;
    Object.keys(appointment).forEach(e => {
      var cell = row.insertCell(row.cells.length);
      cell.innerHTML = appointment[e];
    });
    row.classList += "clickable-row";
  });
  setActions();

}
function setActions(){
  var rows = document.getElementsByClassName('clickable-row');
  $('.clickable-row').on("click",function(e){
    //get value in table
    getDocumentsByAppointments(document.getElementById("t-appointments").rows[$(this).index()+1].cells[0].innerHTML);
    jQuery.noConflict();     
  });
  
}

function removeRows(){
  while (table.rows.length > INDEX_G)table.deleteRow(INDEX_G);
}

function cleanAppointmentsTable() {
    let default_row = 3;
    let nro_col = 6;
    
    for (let index = INDEX_G; index <= default_row; index++) {
      var row = table.insertRow(index);
      row.classList.add('table-row');
    
      for (let i = 0; i < nro_col; i++) {
        var cell = row.insertCell(i);
        cell.innerHTML = " ";
      }
      
    }

}


(function() {
  'use strict';
  window.addEventListener('load', function() {
      
      //fillSpecialites();
      var user = JSON.parse(localStorage.getItem("user"));
      document.getElementById('s-welcome').innerHTML = "Hola! "+user.firstName+" "+user.lastName;
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
    
      document.getElementById("b-logout").addEventListener("click", function () {
        localStorage.clear();
        document.location.href = './../index.html';
      });
   
    var forms = document.getElementsByClassName('needs-validation');
    var s_label = document.getElementById('s-label');
    
    
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
          
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
        if (form.checkValidity() === true) {
          //todo call service
          event.preventDefault();
          s_label.value = "Buscando...";
          s_label.classList.add('spinner-border');
          
          searchAppointments(document.getElementById("i-dni").value,s_label);            
        }
          
        
      }, false);
    });
  }, false);
})();