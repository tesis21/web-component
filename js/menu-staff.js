const INDEX_G = 1;
const STATUS_OK = 200;
const MAX_LENGTH_DNI = 8;
const FAKE_API = "http://localhost:3000/";
const MIN_LENGTH_MEDICINE = 7;
//const API = "http://localhost:8081/api/v1/";
const API = "https://api-rest-tesis2.herokuapp.com/api/v1/";
const TOKEN = localStorage.getItem("token").replace(/"/g, "");

const dayNames = "DOMINGO,LUNES,MARTES,MIÉRCOLES,JUEVES,VIERNES,SÁBADO".split(",")
const days = [...Array(32).keys()].splice(1); // generate 1-31
var global_index_appointment = 0;
var global_office = 0;
var global_init = "";
var global_floor = 0;


var table = document.getElementById("t-appointments");
var table_availability = document.getElementById("t-availability");
var t_availability_result = document.getElementById("t-availability-result");

var inputDni = document.getElementById("inputDni");
var inputDniModal = document.getElementById("inputDniModal");
var inputUser = document.getElementById("inputUser");
var inputUserModal = document.getElementById("inputUserModal");

var user_notification = document.getElementById("user-notification");

var userLoader = document.getElementById("userLoader");
userLoader.hidden = true;
var userLoaderModal = document.getElementById("userLoaderModal");
userLoaderModal.hidden = true;

var appointment_tab = document.getElementById("appointment-tab");
var inputDate = document.getElementById("inputDate");




const selectSpecialties = document.getElementById("selectSpecialties");
const selectDoctors = document.getElementById("selectDoctors");

var list_data_doctors = [];

var spec_selected = 0;
var doc_selected = 0;

const getDates = (yyyy, mm, dayName) => {
  const dayNumber = dayNames.indexOf(dayName);
  mm -= 1; // JS months start at 0
  return days.filter(d => {
    const date = new Date(yyyy, mm, d);
    return mm === date.getMonth() && dayNumber === date.getDay()
  });
};

console.log(
  getDates(2020, 9, "Wednesday")
)

function reserve() {
  //global_index_appointment

  var xhr = new XMLHttpRequest();
  xhr.open("POST", API + 'medicalregister', true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.setRequestHeader('Authorization', 'Bearer ' + TOKEN);

  var dt = inputDate.value.split('-');
  var dtd = dt[2] + '/' + dt[1] + '/' + dt[0];

  var usr = JSON.parse(localStorage.getItem("registro_cita_user"));

  xhr.send(JSON.stringify({
    date: dtd,
    user_id: usr.id,
    doctor_id: doc_selected.id,
    name: "RESERVA DE CITA",
    prescription: "",
    comments: "",
    medicine_id: null,
    appointment_id: null,
    office: global_office,
    floor: global_floor,
    init: global_init

  }));
  xhr.onload = function () {
    if (this.status === 403) {
      console.log('error');
    }
    if (this.status === 200) {
      var data = JSON.parse(this.responseText);
      console.log(data);
      if (data !== "undefined") {
        alert('Guardado con éxito');
        location.reload();
      } else {
        alert('Error de conexion');
      }


    }
    $('#md-close').trigger('click');
  }
  xhr.onerror = function () { alert('Error de conexion'); };


}

//Set listeners
inputDni.addEventListener('keyup', getUser);
$("#inputDniModal").keyup(function () {
  getUserModal();
});

user_notification.addEventListener('click', (event) => {
  var xhr = new XMLHttpRequest();
  xhr.open("POST", API + 'notification/token/user', true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.setRequestHeader('Authorization', 'Bearer ' + TOKEN);
  var usr = JSON.parse(localStorage.getItem("registro_permisos_user"));
  xhr.send(JSON.stringify({
    title: "SOLICITUD DE PERMISOS",
    message: "LA CLÍNICA X REQUIERE SUS SERVICIOS",
    topic: "TOPIC",
    token: "token",
    user_id: usr.id
  }));
  xhr.onload = function () {
    if (this.status === 403) {
      console.log('error');
    }
    if (this.status === 200) {
      var data = JSON.parse(this.responseText);

      if (data.status == STATUS_OK) {
        alert('El permiso ha sido enviado');
        location.reload();
      } else {
        alert('Error de conexion');
      }


    }

  }
  xhr.onerror = function () { alert('Error de conexion'); };


});
function is_valid_dayname(doc_selected, day_name) {
  for (let index = 0; index < doc_selected.schedules.length; index++) {
    if (doc_selected.schedules[index].day === day_name) return index;
  }
  return -1;
}
$('#appointment-tab').click(function (event) {

  var usr = localStorage.getItem("registro_cita");
  if (typeof usr == 'undefined' || !usr) return false;
  if (inputDate.value === "") return false;
  if (typeof doc_selected == 'undefined' || !doc_selected || doc_selected == "") return false;
  if (typeof spec_selected == 'undefined' || !spec_selected || spec_selected == "") return false;

  var dt = inputDate.value.split('-');
  var dtd = dt[2] + '/' + dt[1] + '/' + dt[0];

  var day_name = new Date(dt[1] + '/' + dt[2] + '/' + dt[0]).toLocaleString('es-MX', { weekday: 'long' });
  day_name = day_name.toUpperCase();
  var index_med;
  if ((index_med = is_valid_dayname(doc_selected, day_name)) == -1) return false;

  $('#day_selected').text("Cupos Disponibles para el : " + day_name + " " + dtd);
  var spec_name = $("#selectSpecialties option[value='" + spec_selected + "']").text();
  $('#day_detail').text(spec_name + " - " + doc_selected.name);


  var init = parseInt(doc_selected.schedules[index_med].init_date);
  var dummy_list = [];
  //let init = Math.floor(Math.random() * (6) + 7);
  for (let index = init; index < init + 6; index++) {
    var sched = {
      a: (index < 10 ? '0' + index : index) + ':00',
      b: ((index + 1) < 10 ? '0' + (index + 1) : (index + 1)) + ':00',
      c: doc_selected.schedules[0].office,
      d: 'PISO ' + doc_selected.schedules[0].floor
    }
    dummy_list.push(sched);
  }
  var indexx = 0;
  dummy_list.forEach(appointment => {
    var row = t_availability_result.insertRow(t_availability_result.rows.length);
    row.value = appointment.id;
    row.className = "tr-alt";
    row.id = "table-index" + indexx;
    //adding first 4 td
    Object.keys(appointment).forEach(e => {
      var cell = row.insertCell(row.cells.length);
      cell.innerHTML = appointment[e];
      cell.className = "td-alt";
    });
    //adding the 5th
    var cell = row.insertCell(row.cells.length);
    cell.innerHTML = '<button class="btn btn-secondary btn-alt showmodal" data-toggle="modal" data-target="#conAppModal" id="' + indexx + '">Reservar</button>';
    indexx++;
  });

  $('.showmodal').click(function () {
    global_index_appointment = this.id;

    var columns = [];
    var row_id = "#table-index" + this.id;
    $(row_id).find('td').each(function () {
      columns.push($(this).text());
    });
    console.log(columns);

    global_office = parseInt(columns[2]);
    global_init = columns[0];
    var alt_floor = columns[3].split(' ');

    global_floor = parseInt(alt_floor[1]);

  });

  t_availability_result.deleteRow(INDEX_G);
  $('.btn-alt').click(function () {
    //this action get the data to reserve appointment and show modal
    var list_content = $(this).closest("tr").find(".td-alt");
    for (let index = 0; index < list_content.length; index++) {

    }

    // Finds the closest row <tr> 
    // Gets a descendent with class="nr"

  });


});


selectDoctors.addEventListener('change', (event) => {
  if (event.target.value.length > 3) {
    cleanSelectDoctor();
    return;
  }

  var doctor_data = list_data_doctors.filter(obj => {
    return obj.id == event.target.value;
  });
  doc_selected = doctor_data[0];
  fillTableSchedules(doctor_data[0].schedules);
});
selectSpecialties.addEventListener('change', (event) => {
  if (event.target.value.length > 3) return;
  spec_selected = event.target.value;
  var xhr = new XMLHttpRequest();
  xhr.open("GET", API + 'doctor/specialty/' + event.target.value, true);
  //xhr.open("GET", FAKE_API+'doctors?specialty='+event.target.value, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.setRequestHeader('Authorization', 'Bearer ' + TOKEN);
  xhr.send();
  xhr.onload = function () {
    userLoader.hidden = true;
    if (this.status === 403) {
      console.log('error');
    }
    if (this.status === 200) {
      let data = JSON.parse(this.responseText);

      if (typeof data !== 'undefined' && data.length > 0) {
        cleanSelectDoctor();
        list_data_doctors = data;
        for (var item of data) {
          var e = document.createElement("option");
          e.innerText = item.name;
          e.value = item.id;
          selectDoctors.append(e);
        }
      }
    }

  }

});

function fillTableSchedules(data) {
  console.log('calling.... fillTableSchedules');
  if (data.length === 0 || data === "" || data == NaN) {
    return;
  }

  data.forEach(appointment => {
    var row = table_availability.insertRow(table_availability.rows.length);
    row.value = appointment.id;
    Object.keys(appointment).forEach(e => {
      var cell = row.insertCell(row.cells.length);
      cell.innerHTML = appointment[e];
    });
    row.classList += "clickable-row";
  });
  table_availability.deleteRow(INDEX_G);
}

function cleanSelectDoctor() {
  let size = selectDoctors.options.length;
  for (i = size - 1; i > 0; i--) {
    selectDoctors.options[i] = null;
  }
}
function fillSpecialites() {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", API + 'specialties', true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.setRequestHeader('Authorization', 'Bearer ' + TOKEN);
  xhr.send();
  xhr.onload = function () {
    if (this.status === 403) {
    }
    if (this.status === 200) {
      let data = JSON.parse(this.responseText);

      if (typeof data !== 'undefined' && data.length > 0) {

        for (var item of data) {
          var e = document.createElement("option");
          e.innerText = item.title;
          e.value = item.id;
          selectSpecialties.append(e);
        }
      }
    }

  }
}

function getUser() {
  if (inputDni.value.length == MAX_LENGTH_DNI) {
    userLoader.hidden = false;
    queryUserDb(inputDni.value, inputUser, userLoader, 'cita');
  }

}
function getUserModal() {
  if (inputDniModal.value.length == MAX_LENGTH_DNI) {
    userLoaderModal.hidden = false;
    queryUserDb(inputDniModal.value, inputUserModal, userLoaderModal, 'permisos');
  }

}


function queryUserDb(dni, out, loader, type) {
  var xhr = new XMLHttpRequest();
  //xhr.open("GET", 'http://localhost:3000/patients?dni='+dni, true);
  var route = API + 'patients/dni/' + dni;
  if (type === 'cita') {
    route = API + 'patients/dni/' + dni;
  } else {
    route = API + 'patients/dni-free/' + dni;
  }
  xhr.open("GET", route, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.setRequestHeader('Authorization', 'Bearer ' + TOKEN);
  xhr.send();
  xhr.onload = function () {
    loader.hidden = true;
    if (this.status === 403) {
      console.log('error');
    }
    if (this.status === 200) {
      let data = JSON.parse(this.responseText);
      console.log(data);
      if (type === 'cita') {
        localStorage.setItem("registro_cita", true);
        localStorage.setItem("registro_cita_user", JSON.stringify(data));
      } else {
        localStorage.setItem("registro_permisos", true);
        localStorage.setItem("registro_permisos_user", JSON.stringify(data));
      }

      if (typeof data !== 'undefined') {
        out.value = data.name;
      } else {
        alert('No se encontró el paciente');
      }

    }

  }
}

function searchAppointments(dni, label) {
  //http://localhost:8284/api/v1/
  var xhr = new XMLHttpRequest();
  //xhr.open("GET", 'http://localhost:3000/appointments?dni='+dni, true);
  xhr.open("GET", API + 'appointment/dni/' + dni, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.setRequestHeader('Authorization', 'Bearer ' + TOKEN);



  xhr.send();
  xhr.onload = function () {
    if (this.status === 403) {
      console.log('error');
    }
    if (this.status === 200) {
      let data = JSON.parse(this.responseText);
      label.value = "Buscar";
      label.classList.remove('spinner-border');

      fillAppointmentTable(data);
      table.deleteRow(INDEX_G);

    }

  }
  xhr.onerror = function () { alert('Error de conexion'); };

}
function fillDocumentsModal(data) {
  let table = document.getElementById("t-modal-documents");
  console.log(table);
  //remove when services are ready
  data.forEach(element => {
    delete element.appointment;
  });
  let count = 1;
  data.forEach(appointment => {
    console.log(appointment);
    var tr = table.insertRow();
    var td1 = tr.insertCell();
    td1.appendChild(document.createTextNode(count));
    var td2 = tr.insertCell();
    td2.appendChild(document.createTextNode(appointment.name));
    var td3 = tr.insertCell();
    //td3.appendChild('<a href="http://www.africau.edu/images/default/sample.pdf" type="button" class="btn btn-primary" target="_blank"><i class="far fa-eye"></i></a>');
    var createA = document.createElement('a');
    createA.className = "btn btn-primary";
    var createAText = document.createTextNode("Documento");
    //var href = document.createElement('a');
    createA.setAttribute('href', appointment.url);
    createA.appendChild(createAText);
    td3.appendChild(createA);


    //td3.appendChild(href);
    //cb.type = 'checkbox';
    //cb.id = file.id;
    count++;

    /*var row = table.insertRow(table.rows.length);
    row.value = appointment.id;
    Object.keys(appointment).forEach(e => {
      var cell = row.insertCell(row.cells.length);
      cell.innerHTML = appointment[e];
    });*/
    //row.classList += "clickable-row";
  });
}
function getDocumentsByAppointments(id_appointment) {
  //TODO set up service {documents in modal}
  var xhr = new XMLHttpRequest();
  xhr.open("GET", API + 'records/appointment/' + id_appointment, true);
  //xhr.open("GET", 'http://localhost:3000/documents?appointment='+id_appointment,true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.setRequestHeader('Authorization', 'Bearer ' + TOKEN);
  xhr.send();
  xhr.onload = function () {
    if (this.status === 403) {
      console.log('error');
    }
    if (this.status === 200) {
      let data = JSON.parse(this.responseText);

      if (data.length === 0 || data === "" || data == NaN) {
        $('#moda_empty_documents').modal('toggle');
      } else {
        fillDocumentsModal(data);
        $('#registersModal').modal('toggle');
      }

    }

  }
  xhr.onerror = function () { alert('Error de conexion'); };

}

function fillAppointmentTable(data) {
  console.log('calling...fillAppointmentTable');
  if (data.length === 0 || data === "" || data == NaN) {
    alert('No hay información disponible');
  }

  //remove when services are ready
  data.forEach(element => {
    delete element.dni;
  });

  data.forEach(appointment => {

    var row = table.insertRow(table.rows.length);
    row.value = appointment.id;
    Object.keys(appointment).forEach(e => {
      var cell = row.insertCell(row.cells.length);
      cell.innerHTML = appointment[e];
    });
    row.classList += "clickable-row";
  });
  setActions();

}
function setActions() {
  console.log('calling... setActions');
  var rows = document.getElementsByClassName('clickable-row');
  console.log(rows);
  $('.clickable-row').on("click", function (e) {
    console.log('test');
    //get value in table
    getDocumentsByAppointments(document.getElementById("t-appointments").rows[$(this).index() + 1].cells[0].innerHTML);
    jQuery.noConflict();
  });

}

function removeRows() {
  while (table.rows.length > INDEX_G) table.deleteRow(INDEX_G);
}

function cleanAppointmentsTable() {
  let default_row = 3;
  let nro_col = 6;

  for (let index = INDEX_G; index <= default_row; index++) {
    var row = table.insertRow(index);
    row.classList.add('table-row');

    for (let i = 0; i < nro_col; i++) {
      var cell = row.insertCell(i);
      cell.innerHTML = " ";
    }

  }

}


(function () {
  'use strict';
  window.addEventListener('load', function () {

    var user = JSON.parse(localStorage.getItem("user"));
    console.log(user);
    document.getElementById('s-welcome').innerHTML = "Hola! " + user.firstName + " " + user.lastName;
    // Fetch all the forms we want to apply custom Bootstrap validation styles to

    document.getElementById("b-logout").addEventListener("click", function () {
      //TODO logout
      localStorage.clear();
      document.location.href = './../index.html';
    });
    fillSpecialites();

    var forms = document.getElementsByClassName('needs-validation');
    var s_label = document.getElementById('s-label');


    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {

        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
        if (form.checkValidity() === true) {
          //todo call service
          event.preventDefault();
          s_label.value = "Buscando...";
          s_label.classList.add('spinner-border');

          searchAppointments(document.getElementById("i-dni").value, s_label);
        }


      }, false);
    });
  }, false);
})();